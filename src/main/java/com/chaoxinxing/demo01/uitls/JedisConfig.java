package com.chaoxinxing.demo01.uitls;


import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableConfigurationProperties(JedisConfig.JedisProperties.class)
public class JedisConfig {

    @Autowired
    private JedisProperties prop;

    @Bean
    public JedisPool jedisPool() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(500);
        config.setMaxIdle(500);
        config.setMaxWaitMillis(60);
        JedisPool jedisPool;
        if("".equals(prop.getPassword())){
            jedisPool = new JedisPool(config, prop.getHost(), prop.getPort(), prop.getTimeOut());
        }else{
            jedisPool = new JedisPool(config, prop.getHost(), prop.getPort(), prop.getTimeOut(), prop.getPassword());
        }
        return jedisPool;
    }

    @ConfigurationProperties(prefix = JedisProperties.JEDIS_PREFIX)
    @Data
    class JedisProperties {

        public static final String JEDIS_PREFIX = "spring.redis";

        private String host;

        private int port;

        private String password;

        private int maxTotal;

        private int maxIdle;

        private int maxWaitMillis;

        private int timeOut;


    }

}