package com.chaoxinxing.demo01.rabbit;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class MyListener {

    @RabbitListener(queues = "queue")
    public void msg(String msg){
        System.out.println("消费者消费消息了："+msg);
        //TODO 这里可以做异步的工作
    }
}