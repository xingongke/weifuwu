package com.chaoxinxing.demo01.dao;

import com.chaoxinxing.demo01.bean.Book;
import com.chaoxinxing.demo01.bean.BookBean;

import com.chaoxinxing.demo01.mapper.Book2Mapper;
import com.chaoxinxing.demo01.mapper.Book3Mapper;
import com.chaoxinxing.demo01.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CrudDao {


    @Autowired
  private Book3Mapper book3Mapper;

    @Autowired
  private Book2Mapper book2Mapper ;

    @Autowired
  private BookMapper bookMapper ;


    public  String insertDaoBook (BookBean bookBean){

        System.out.println("我们完成了书本的新增");

        int i =  book3Mapper.insertBook(bookBean) ;

        return "我们完成了书本的新增" ;
    }

    public List<BookBean> selectAllBook() {
       List<BookBean> bookBeanList =  book3Mapper.selectAllBook() ;
        return bookBeanList ;
    }

    public String deleteBookById(String id) {

        int i =  book3Mapper.deleteBookById(id) ;
        return "删除了书本id"+id ;
    }

    public String updateBook(BookBean bookBean) {

        int i = book3Mapper.updateBook(bookBean) ;

        return "修改了书本id"+bookBean.getId() ;



    }


    public  String insertDaoBook2(BookBean bookBean){

        System.out.println("我们完成了书本的新增");

        int i =  book2Mapper.insert(bookBean) ;

        return "我们完成了书本的新增" ;
    }


    public  String insertBook3(Book book){

        System.out.println("我们完成了书本的新增");

        int i =  bookMapper.insert(book) ;

        return "我们完成了书本的新增" ;
    }


}
