package com.chaoxinxing.demo01.bean;

import java.util.Date;
import javax.persistence.*;

@Table(name = "book")
public class Book {
    /**
     * id
     */
    @Id
    private Long id;

    /**
     * 书本名称
     */
    private String name;

    /**
     * 作者
     */
    private String auth;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 1:删除 0：正常
     */
    private Integer status;

    /**
     * 获取id
     *
     * @return id - id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取书本名称
     *
     * @return name - 书本名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置书本名称
     *
     * @param name 书本名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取作者
     *
     * @return auth - 作者
     */
    public String getAuth() {
        return auth;
    }

    /**
     * 设置作者
     *
     * @param auth 作者
     */
    public void setAuth(String auth) {
        this.auth = auth;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取1:删除 0：正常
     *
     * @return status - 1:删除 0：正常
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置1:删除 0：正常
     *
     * @param status 1:删除 0：正常
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
}