package com.chaoxinxing.demo01.bean;

import lombok.Data;

import javax.persistence.Table;


// 书本实体
@Data
@Table(name = "book")
public class BookBean {


     private  long id ;

    // 书名
    private String  name ;
    // 作者

    private String  auth ;



}
