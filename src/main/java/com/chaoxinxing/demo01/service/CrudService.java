package com.chaoxinxing.demo01.service;

import com.chaoxinxing.demo01.bean.Book;
import com.chaoxinxing.demo01.bean.BookBean;
import com.chaoxinxing.demo01.dao.CrudDao;
//import com.chaoxinxing.demo01.rabbit.RabbitMQConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@CacheConfig(cacheManager = "cacheManager", cacheNames = {"CrudService"})
public class CrudService {

  @Autowired
  private CrudDao crudDao ;

  @Autowired
  private RedisTemplate redisTemplate ;


  public  String   insertServiceBook(BookBean bookBean){
   return   crudDao.insertDaoBook(bookBean) ;
  }


    @Cacheable(value = "selectAllBook")
    public List<BookBean> selectAllBook( ) {

      redisTemplate.opsForValue().set("111","222");



      return   crudDao.selectAllBook() ;
    }

    public String deleteBookById(String id) {


            return   crudDao.deleteBookById(id) ;
    }

    public String updateBook(BookBean bookBean) {
        return   crudDao.updateBook(bookBean) ;

    }

    public String insertServiceBook2(BookBean bookBean) {

        return   crudDao.insertDaoBook2(bookBean) ;

    }

    public String insertBook3(Book book) {
        return   crudDao.insertBook3(book) ;
    }

  //注入RabbitMQ的模板
  @Autowired
  private RabbitTemplate rabbitTemplate;

    public Object sendMsg(String msg) {

/**
 * 发送消息
 * 参数一：交换机名称
 * 参数二：路由key: item.springboot-rabbitmq,符合路由item.#规则即可
 * 参数三：发送的消息
 */


      String key  = "item";
      rabbitTemplate.convertAndSend("directExchange" ,key ,msg);
      //返回消息
      return "发送消息成功！";

    }
}
