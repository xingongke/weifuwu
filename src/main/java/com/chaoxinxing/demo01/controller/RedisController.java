package com.chaoxinxing.demo01.controller;


import com.chaoxinxing.demo01.bean.Book;
import com.chaoxinxing.demo01.bean.BookBean;
import com.chaoxinxing.demo01.service.CrudService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@Controller

@RestController
//@RequestMapping("/demo01_")
@Api(tags = "redis模块")

public class RedisController {

    @Autowired //自动注入
    private CrudService crudService;


    @ApiOperation("查询书本列表")
    @GetMapping("/selectBookList")
    @ResponseBody
    public Object selectBookList( ) {
        return crudService.selectAllBook();
    }



}
