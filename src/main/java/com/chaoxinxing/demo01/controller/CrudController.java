package com.chaoxinxing.demo01.controller;


import com.chaoxinxing.demo01.bean.Book;
import com.chaoxinxing.demo01.bean.BookBean;
import com.chaoxinxing.demo01.service.CrudService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@Controller

@RestController

@Api(tags = "增删改查模块")

public class CrudController {

    @Autowired //自动注入
    private CrudService crudService;

    @ApiOperation("新增接口")
    @GetMapping("/insertBook")
    @ResponseBody   // 数据结构的方式返回
    public String insertBook(BookBean bookBean) {
        return crudService.insertServiceBook(bookBean);
    }

    @ApiOperation("查询接口")
    @GetMapping("/selectAllBook")
    @ResponseBody   // 数据结构的方式返回
    public List<BookBean> selectAllBook( ) {
        return crudService.selectAllBook();
    }

    @ApiOperation("删除接口")
    @GetMapping("/deleteBookById")
    @ResponseBody   // 数据结构的方式返回
    public String deleteBookById(String id) {
        return crudService.deleteBookById(id);
    }

    @ApiOperation("修改接口")
    @GetMapping("/updateBook")
    @ResponseBody   // 数据结构的方式返回
    public String updateBook(BookBean bookBean) {
        return crudService.updateBook(bookBean);
    }

    @ApiOperation("新增接口")
    @GetMapping("/insertBook2")
    @ResponseBody   // 数据结构的方式返回
    public String insertBook2(BookBean bookBean) {
        return crudService.insertServiceBook2(bookBean);
    }



    @ApiOperation("新增接口3")
    @GetMapping("/insertBook3")
    @ResponseBody   // 数据结构的方式返回
    public String insertBook3(Book book) {
        return crudService.insertBook3(book);
    }



}
