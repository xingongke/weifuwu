package com.chaoxinxing.demo01.controller;


import com.chaoxinxing.demo01.service.CrudService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@Controller

@RestController
//@RequestMapping("/demo01_")
@Api(tags = "rabbitmq模块")

public class RabbitmqController {

    @Autowired //自动注入
    private CrudService crudService;


    @ApiOperation("发送消息")
    @GetMapping("/sendMsg")
    @ResponseBody
    public Object sendMsg( ) {
        return crudService.sendMsg("hello world");
    }



}
