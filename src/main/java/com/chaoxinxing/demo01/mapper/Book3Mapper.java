package com.chaoxinxing.demo01.mapper;

import com.chaoxinxing.demo01.bean.BookBean;

import java.util.List;

public interface Book3Mapper {


    int insertBook(BookBean bookBean)  ;

    List<BookBean> selectAllBook();

    int deleteBookById(String id);

    int updateBook(BookBean bookBean);
}
