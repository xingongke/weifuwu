package com.chaoxinxing.demo01.mapper;

import com.chaoxinxing.demo01.bean.Book;
import tk.mybatis.mapper.common.Mapper;

public interface BookMapper extends Mapper<Book> {
}